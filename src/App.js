import { jsPlumb } from "jsplumb";
import { useEffect, useRef, useState } from "react";

const style = {
  width: 100,
  height: 100,
  background: "#e0e0e0",
};
const App = () => {
  const [elements, setElements] = useState([]);
  const [endpoints, setEndpoints] = useState([]);
  const [connections, setConnections] = useState([]);
  const jsp = useRef();
  const graph = useRef();

  useEffect(() => {
    if (jsp.current) {
      const elementsIds = [];
      elements.forEach((el) => {
        if (endpoints.indexOf(el.id) === -1) {
          elementsIds.push(el.id);
        }
      });
      jsp.current.addEndpoint(elementsIds, {
        isSource: false,
        isTarget: true,
        maxConnections: 1,
        anchor: "Top",
        connector: ["Straight", { stub: "25px", gap: 0 }],
      });

      jsp.current.addEndpoint(elementsIds, {
        isSource: true,
        isTarget: false,
        maxConnections: 1,
        anchor: "Bottom",
        connector: ["Straight", { stub: 25, gap: 0 }],
      });
      setEndpoints([...endpoints, ...elementsIds]);
      jsp.current.draggable(elementsIds, {
        containment: graph.current.id,
      });
    }
  }, [elements]);

  useEffect(() => {
    jsp.current = jsPlumb.getInstance({
      Anchor: "AutoDefault",
      Container: graph.current,
    });
    connections.forEach((connection) =>
      jsp.current.connect({
        source: connection.source,
        target: connection.target,
        connector: connection.connection.connector,
      })
    );
  }, []);

  useEffect(() => {
    if (jsp.current) {
      jsp.current.unbind("connection");
      jsp.current.bind("connection", (connect) => {
        setConnections([...connections, connect]);
      });
      jsp.current.unbind("beforeDrop");
      jsp.current.bind("beforeDrop", function (info) {
        if (info.sourceId === info.targetId) {
          console.log(
            "source and target ID's are the same - self connections not allowed."
          );
          return false;
        } else {
          return true;
        }
      });
    }
  });

  const handleAddElement = () => {
    const id = `Element-${Math.floor(Math.random() * (999 - 1) + 1)}`;
    const newElement = {
      left: Math.floor(Math.random() * (999 - 0) + 0),
      top: Math.floor(Math.random() * (500 - 0) + 0),
      id,
    };
    setElements([...elements, newElement]);
  };

  const handleDeleteElement = (e) => {
    const id = e.target.parentNode.id;
    jsp.current.getEndpoints(e.target.parentNode).forEach((ep) => {
      jsp.current.deleteEndpoint(ep);
    });
    const endpointsFiltered = endpoints.filter((ep) => ep !== id);
    const elementsFiltered = elements.filter((el) => el.id !== id);
    const connectionsFiltered = connections.filter(
      (conn) => conn.sourceId !== id && conn.targetId !== id
    );
    setEndpoints(endpointsFiltered);
    setElements(elementsFiltered);
    setConnections(connectionsFiltered);
  };

  return (
    <div
      ref={graph}
      style={{ position: "relative", height: 600 }}
      id="container"
    >
      <button onClick={handleAddElement}>Añadir elemento</button>
      {elements.map((element, i) => (
        <div
          id={element.id}
          key={element.id}
          className="element"
          style={{
            ...style,
            position: "absolute",
            left: element.left,
            top: element.top,
          }}
        >
          <button onClick={handleDeleteElement}>x</button>
        </div>
      ))}
    </div>
  );
};

export default App;
